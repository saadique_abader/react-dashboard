
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mysql = require('mysql');
 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
 
// connection configurations
    const connection = mysql.createConnection({ multipleStatements: true,
    host: 'storm.zieto.co.za',
    user: 'root',
    password: 'mogammad',
    database: 'Service_enquiries',
    root: '3306'
});
 
// connect to database
connection.connect();
 
// default route
app.get('/', function (req, res) {
    return res.send({ error: true, message: 'hello' })
});
 
// Retrieve all Member details 
app.get('/querydetails', function (req, res) {
    connection.query('SELECT * FROM Service_enquiries.Query_details', function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Query details list.' });
    });
});
 
// Search for Query details using Reference_ID
app.get('/querydetails/refidsearch/:keyword', function (req, res) {
    let keyword = req.params.keyword;
    connection.query("SELECT * FROM Service_enquiries.Query_details WHERE Reference_ID LIKE ? ", ['%' + keyword + '%'], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Query details search list.' });
    });
});

// Search for Query details using Reference_ID
app.get('/querydetails/refsearch', function (req, res) {
    connection.query("SELECT Provider_No FROM Service_enquiries.Query_details;", function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Query details search list.' });
    });
});
 
// Search for Query details using Member_No
app.get('/querydetails/memnosearch/:keyword', function (req, res) {
    let keyword = req.params.keyword;
    connection.query("SELECT * FROM Service_enquiries.Query_details WHERE Member_No LIKE ? ", ['%' + keyword + '%'], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Query details search list.' });
    });
});

// Search for Query details using Provider_No
app.get('/querydetails/provnosearch/:keyword', function (req, res) {
    let keyword = req.params.keyword;
    connection.query("SELECT * FROM Service_enquiries.Query_details WHERE Provider_No LIKE ? ", ['%' + keyword + '%'], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Query details search list.' });
    });
});


// Search for Query details using Phone_Number
app.get('/querydetails/phonenosearch/:keyword', function (req, res) {
    let keyword = req.params.keyword;
    connection.query("SELECT * FROM Service_enquiries.Query_details WHERE Phone_Number LIKE ? ", ['%' + keyword + '%'], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Query details search list.' });
    });
});

// Search for Query details using Type pf Enquiry
app.get('/querydetails/typeenquirysearch/:keyword', function (req, res) {
    let keyword = req.params.keyword;
    connection.query("SELECT * FROM Service_enquiries.Query_details WHERE Type_of_Enquiry LIKE ? ", ['%' + keyword + '%'], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Query details search list.' });
    });
});

// Search for Query details using Employee_ID
app.get('/querydetails/employeeidsearch/:keyword', function (req, res) {
    let keyword = req.params.keyword;
    connection.query("SELECT * FROM Service_enquiries.Query_details WHERE Employee_ID LIKE ? ", ['%' + keyword + '%'], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Query details search list.' });
    });
});

//rest api to create a new record into mysql database
app.post('/querydetails/insert', function (req, res) {
   var postData  = req.body;
   connection.query('INSERT INTO Service_enquiries.Query_details SET ?', postData, function (error, results, fields) {
   if (error) throw error;
   res.end(JSON.stringify(results));
 });
});


//updating query_details tables
app.put('/querydetails/update', function (req, res) {
   connection.query('UPDATE Service_enquiries.Query_details SET Member_No=?,Provider_No=?,Phone_Number=?,Type_of_Enquiry=?,Query_Details=?,StartTime=?, EndTime=?, Employee_ID=? where Reference_ID=?', [req.body.Member_No,req.body.Provider_No, req.body.Phone_Number, req.body.Type_of_Enquiry, req.body.Query_Details, req.body.StartTime, req.body.EndTime,req.body.Employee_ID, req.body.Reference_ID], function (error, results, fields) {
   if (error) throw error;
   res.end(JSON.stringify(results));
 });
});
 
//deleting from query_details
app.delete('/querydetails/delete', function (req, res) {
    console.log(req.body);
    connection.query('DELETE FROM Service_enquiries.Query_details WHERE Reference_ID=?', [req.body.Reference_ID], function (error, results, fields) {
    if (error) throw error;
    res.end('Record has been deleted!');
  });
 });

 // Retrieve all Login_details
app.get('/logindetails', function (req, res) {
    connection.query('SELECT * FROM Service_enquiries.Login_details', function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Login details list.' });
    });
 });
 
 // To add new Employee_id and Password to database
 app.post('/logindetails/insert', function (req, res) {
    var postData  = req.body;
    connection.query('INSERT INTO Service_enquiries.Login_details SET ?', postData, function (error, results, fields) {
    if (error) throw error;
    res.end(JSON.stringify(results));
 });
 });
 

 
 // To search for Login_details using Password
 app.get('/logindetails/password/:username/:password', function (req, res) {
    let username = req.params.username;
    let password = req.params.password;

    connection.query("SELECT * FROM Service_enquiries.Login_details WHERE Employee_id='" + username + "' AND Passwords='" + password + "'",function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Login details list.' });
    });

 });
 
// all other requests redirect to 404
app.all("*", function (req, res, next) {
    return res.send('page not found');
    next();
});
 
// port must be set to 8585 because incoming http requests are routed from port 80 to port 8585
app.listen(8585, function () {
    console.log('Node app is running on port 8585');
});